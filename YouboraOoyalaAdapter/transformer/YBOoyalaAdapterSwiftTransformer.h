//
//  YBOoyalaAdapterSwiftTransformer.h
//  YouboraOoyalaAdapter
//
//  Created by nice on 29/01/2020.
//  Copyright © 2020 NPAW. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "YBOoyalaAdapter.h"
#import "YBOoyalaAdsAdapter.h"

@interface YBOoyalaAdapterSwiftTransformer : NSObject

+(YBPlayerAdapter<id>*)transformFromAdapter:(YBOoyalaAdapter*)adapter;
+(YBPlayerAdapter<id>*)transformFromAdsAdapter:(YBOoyalaAdsAdapter*)adapter;

@end
