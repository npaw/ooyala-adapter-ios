//
//  YBOoyalaAdapterSwiftTransformer.m
//  YouboraOoyalaAdapter
//
//  Created by nice on 29/01/2020.
//  Copyright © 2020 NPAW. All rights reserved.
//

#import "YBOoyalaAdapterSwiftTransformer.h"

@implementation YBOoyalaAdapterSwiftTransformer

+(YBPlayerAdapter<id>*)transformFromAdapter:(YBOoyalaAdapter*)adapter { return adapter; }
+(YBPlayerAdapter<id>*)transformFromAdsAdapter:(YBOoyalaAdsAdapter*)adapter { return adapter; }

@end
