//
//  YouboraOoyalaAdapter.h
//  YouboraOoyalaAdapter
//
//  Created by Enrique Alfonso Burillo on 15/02/2018.
//  Copyright © 2018 NPAW. All rights reserved.
//

#import <UIKit/UIKit.h>

//! Project version number for YouboraOoyalaAdapter.
FOUNDATION_EXPORT double YouboraOoyalaAdapterVersionNumber;

//! Project version string for YouboraOoyalaAdapter.
FOUNDATION_EXPORT const unsigned char YouboraOoyalaAdapterVersionString[];

// In this header, you should import all the public headers of your framework using statements like #import <YouboraOoyalaAdapter/PublicHeader.h>
#import <YouboraOoyalaAdapter/YBOoyalaAdapter.h>
#import <YouboraOoyalaAdapter/YBOoyalaAdsAdapter.h>
#import <YouboraOoyalaAdapter/YBOoyalaAdapterSwiftWrapper.h>
#import <YouboraOoyalaAdapter/YBOoyalaAdapterSwiftTransformer.h>
