//
//  YBOoyalaAdapter.h
//  YouboraOoyalaAdapter
//
//  Created by Enrique Alfonso Burillo on 15/02/2018.
//  Copyright © 2018 NPAW. All rights reserved.
//

@import YouboraLib;

@class OOOoyalaPlayer;

@interface YBOoyalaAdsAdapter : YBPlayerAdapter<OOOoyalaPlayer *>

@end
