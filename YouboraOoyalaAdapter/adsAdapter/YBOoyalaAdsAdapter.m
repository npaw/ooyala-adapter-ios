//
//  YBOoyalaAdapter.m
//  YouboraOoyalaAdapter
//
//  Created by Enrique Alfonso Burillo on 15/02/2018.
//  Copyright © 2018 NPAW. All rights reserved.
//

#import "YBOoyalaAdsAdapter.h"
#import "Utils.h"
// Ooyala imports
#import <OoyalaSDK/OoyalaSDK.h>

@implementation YBOoyalaAdsAdapter

- (void) registerListeners{
    
    [super registerListeners];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(playerNotification:)
                                                 name:nil
                                               object:self.player];
}

- (void) unregisterListeners{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
    
    [super unregisterListeners];
}

- (void) playerNotification:(NSNotification*) notification {
    OOOoyalaPlayer * player = (OOOoyalaPlayer *) self.player;
    
    NSString * notificationName = notification.name;
    
    if ([notificationName isEqualToString:OOOoyalaPlayerTimeChangedNotification]) {
        return;
    }
    
    
    
    [YBLog debug:@"Notification Received (ad adapter): %@. state: %@. playhead: %f", [notification name], [OOOoyalaPlayerStateConverter playerStateToString: player.state], [player playheadTime]];
    
    if ([notificationName isEqualToString:OOOoyalaPlayerAdStartedNotification]){
        [self fireStart];
        [self fireJoin];
    } else if ([notificationName isEqualToString:OOOoyalaPlayerAdCompletedNotification]){
        NSDictionary<NSString*, NSString*> *dict = @{
                                                     @"adPlayhead": @"-1"
                                                     };
        [self fireStop:dict];
    } else if ([notificationName isEqualToString:OOOoyalaPlayerAdSkippedNotification]){
        NSDictionary<NSString*, NSString*> *dict = @{
                                                     @"skipped": @"true"
                                                     };
        [self fireStop:dict];
    } else if ([notificationName isEqualToString:OOOoyalaPlayerStateChangedNotification]){
        if (player.isShowingAd) {
            switch (player.state) {
                case OOOoyalaPlayerStateLoading:
                    [self fireBufferBegin];
                    break;
                case OOOoyalaPlayerStatePlaying:
                    [self fireBufferEnd];
                    [self fireResume];
                    break;
                case OOOoyalaPlayerStatePaused:
                    [self firePause];
                    break;
                case OOOoyalaPlayerStateError:{
                    OOOoyalaError * error = player.error;
                    if (error == nil) {
                        [self fireFatalErrorWithMessage:@"Unknown error" code:@"9000" andMetadata:nil];
                    } else {
                        [self fireFatalErrorWithMessage:error.description code:[NSString stringWithFormat:@"%ld", (long)error.code] andMetadata:nil];
                    }
                    break;
                }
                    
                default:
                    break;
            }
        }
    }
}

- (NSString *)getPlayerVersion {
    return [Utils getPlayerNameForAds:true];
}

- (NSString *)getPlayerName {
    return [Utils getPlayerNameWithOSForAds:true];
}

- (NSString *)getVersion {
    return [Utils getPlayerNameWithOSAndVersionForAds:true];
}

- (NSNumber *)getPlayhead {
    
    NSNumber * playhead = [super getPlayhead];
    
    @try {
        OOOoyalaPlayer * player = self.player;
        if (player.isShowingAd) {
            double ph = player.playheadTime;
            if (ph > 0) {
                playhead = @(ph);
            }
        }
    } @catch (NSException *exception) {
        [YBLog logException:exception];
    } @finally {
        return playhead;
    }
}

- (YBAdPosition) getPosition{
    if(self.plugin != nil && self.plugin.adapter != nil){
        if(!self.plugin.adapter.flags.joined){
            return YBAdPositionPre;
        }
        return YBAdPositionMid;
    }
    return YBAdPositionUnknown;
}

- (NSNumber *) getDuration{
    
    NSNumber* duration = [super getDuration];
    
    @try {
        OOOoyalaPlayer * player = self.player;
        if (player.isShowingAd) {
            double dur = player.duration;
            if (dur > 0) {
                duration = @(dur);
            }
        }
    } @catch (NSException *exception) {
        [YBLog logException:exception];
    } @finally {
        return duration;
    }
}

- (void) fireStart{
    if(self.plugin != nil && self.plugin.adapter != nil){
        [self.plugin.adapter firePause];
    }
    [super fireStart];
}

- (void) fireStop:(NSDictionary<NSString *,NSString *> *)params{
    [super fireStop:params];
    if(self.plugin != nil && self.plugin.adapter != nil){
        [self.plugin.adapter fireResume];
    }
}

@end
