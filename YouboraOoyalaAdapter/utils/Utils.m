//
//  Utils.m
//  YouboraOoyalaAdapter
//
//  Created by Tiago Pereira on 26/02/2020.
//  Copyright © 2020 NPAW. All rights reserved.
//

#import "Utils.h"

#define OS_NAME_IOS "iOS"
#define OS_NAME_TVOS "tvOS"
#define OS_NAME_UNKNOWN "unknown"
#define PLAYER_NAME "Ooyala"

// Constants
#define ADAPTER_STRING_VERSION "6.0.6"

@implementation Utils

+(NSString*)getOSName {
    #ifdef TARGET_OS_IOS
        return @OS_NAME_IOS;
    #elif TARGET_OS_TV
       return @OS_NAME_TVOS;
    #elif
       return @OS_NAME_UNKNOWN;
    #endif
}

+(NSString*)getPlayerNameForAds:(Boolean)ads {
    if (ads) { return [NSString stringWithFormat:@"%@-Ads",@PLAYER_NAME]; }
    
    return @PLAYER_NAME;
}

+(NSString*)getPlayerNameWithOSForAds:(Boolean)ads {
    return [NSString stringWithFormat:@"%@-%@",[self getPlayerNameForAds:ads], [self getOSName]];
}

+(NSString*)getPlayerNameWithOSAndVersionForAds:(Boolean)ads {
    return [NSString stringWithFormat:@"%@-%@", @ADAPTER_STRING_VERSION, [self getPlayerNameWithOSForAds:ads]];
}

@end
