//
//  Utils.h
//  YouboraOoyalaAdapter
//
//  Created by Tiago Pereira on 26/02/2020.
//  Copyright © 2020 NPAW. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Utils : NSObject

+(NSString*)getOSName;
+(NSString*)getPlayerNameForAds:(Boolean)ads;
+(NSString*)getPlayerNameWithOSForAds:(Boolean)ads;
+(NSString*)getPlayerNameWithOSAndVersionForAds:(Boolean)ads;

@end
