//
//  YBOoyalaAdapterSwiftWrapper.h
//  YouboraOoyalaAdapter
//
//  Created by Enrique Alfonso Burillo on 04/04/2018.
//  Copyright © 2018 NPAW. All rights reserved.
//

#import "YBOoyalaAdapter.h"

__attribute__ ((deprecated)) DEPRECATED_MSG_ATTRIBUTE("This class is deprecated. Use YBOoyalaAdapterSwiftTransformer instead")
@interface YBOoyalaAdapterSwiftWrapper : NSObject

- (id) initWithPlayer:(NSObject*)player andPlugin:(YBPlugin*)plugin;
- (void) fireStart;
- (void) fireStop;
- (void) firePause;
- (void) fireResume;
- (void) fireJoin;

- (YBPlugin *) getPlugin;
- (YBOoyalaAdapter * _Nullable) getAdapter;
- (void) removeAdapter;

@end
