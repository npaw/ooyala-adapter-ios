//
//  YBOoyalaAdapterSwiftWrapper.m
//  YouboraOoyalaAdapter
//
//  Created by Enrique Alfonso Burillo on 04/04/2018.
//  Copyright © 2018 NPAW. All rights reserved.
//

#import "YBOoyalaAdapterSwiftWrapper.h"

@interface YBOoyalaAdapterSwiftWrapper()

@property(nonatomic,strong) NSObject* player;
@property(nonatomic,strong) YBPlugin* plugin;
@property(nonatomic,strong) YBOoyalaAdapter* adapter;

@end


@implementation YBOoyalaAdapterSwiftWrapper

- (id) initWithPlayer:(NSObject*)player andPlugin:(YBPlugin*)plugin{
    if (self = [super init]) {
        self.player = player;
        self.plugin = plugin;
    }
    [self initAdapterIfNecessary];
    return self;
}

- (void) fireStart{
    [self initAdapterIfNecessary];
    [self.plugin.adapter fireStart];
}

- (void) fireStop{
    if(self.plugin != nil){
        if(self.plugin.adapter != nil){
            [self initAdapterIfNecessary];
            [self.plugin.adapter fireStop];
            //[self.plugin removeAdapter];
        }
    }
    
}
- (void) firePause{
    [self initAdapterIfNecessary];
    [self.plugin.adapter firePause];
}
- (void) fireResume{
    [self initAdapterIfNecessary];
    [self.plugin.adapter fireResume];
}
- (void) fireJoin{
    [self initAdapterIfNecessary];
    [self.plugin.adapter fireJoin];
}

- (YBOoyalaAdapter *) getAdapter{
    return self.plugin == nil ? nil : (YBOoyalaAdapter *)self.plugin.adapter;
}

- (YBPlugin *) getPlugin{
    return self.plugin;
}

- (void) initAdapterIfNecessary{
    if(self.plugin.adapter == nil){
        if(self.plugin != nil){
            OOOoyalaPlayer* player = (OOOoyalaPlayer*) self.player;
            [self.plugin setAdapter:[[YBOoyalaAdapter alloc] initWithPlayer:player]];
        }
    }
}

- (void) removeAdapter{
    [self.plugin removeAdapter];
}

@end
