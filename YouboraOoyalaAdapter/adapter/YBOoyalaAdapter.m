//
//  YBOoyalaAdapter.m
//  YouboraOoyalaAdapter
//
//  Created by Enrique Alfonso Burillo on 15/02/2018.
//  Copyright © 2018 NPAW. All rights reserved.
//

#import "YBOoyalaAdapter.h"

#import "YBOoyalaAdsAdapter.h"

// Ooyala imports
#import <OoyalaSDK/OoyalaSDK.h>
#import "Utils.h"

@interface YBOoyalaAdapter()

@property (nonatomic, strong) NSNumber * lastPlayhead;

@property (nonatomic, assign) bool contentTreeNotificationReceivedAfterItemChanged;

@property BOOL justSeeked;

@end

@implementation YBOoyalaAdapter

- (void)registerListeners {
    [super registerListeners];
    
    // Init properties
    self.lastPlayhead = [super getPlayhead];
    self.justSeeked = false;
    self.contentTreeNotificationReceivedAfterItemChanged = false;
                         
    [self monitorPlayheadWithBuffers:YES seeks:NO andInterval:400];
    
    // Add player notification observer
    [[NSNotificationCenter defaultCenter] addObserver: self
                                             selector:@selector(playerNotification:)
                                                 name:nil
                                               object:self.player];
    
    // Create Adnalyzer and call startMonitoting on it
    /*self.adnalyzer = [[YBAdnalyzerOoyala alloc] initWithPluginInstance:self];
    [self.adnalyzer startMonitoringWithPlayer:player];*/
}

- (void) unregisterListeners {
    [[NSNotificationCenter defaultCenter] removeObserver:self];
    
    if (self.plugin != nil && self.plugin.adsAdapter != nil) {
        [self.plugin.adsAdapter fireStop];
    }
}

- (void) playerNotification:(NSNotification*) notification {
    
    OOOoyalaPlayer * player = self.player;
    if(self.plugin != nil && self.plugin.adsAdapter == nil){
        [self.plugin setAdsAdapter:[[YBOoyalaAdsAdapter alloc] initWithPlayer:player]];
    }
    
    NSString * notificationName = notification.name;
    
    if ([notificationName isEqualToString:OOOoyalaPlayerTimeChangedNotification]) {
        return;
    }
    
    [YBLog debug:@"Notification Received: %@. state: %@. playhead: %f", [notification name], [OOOoyalaPlayerStateConverter playerStateToString: player.state], [player playheadTime]];
    
    BOOL showingContent = !player.isShowingAd;
    
    if ([notificationName isEqualToString:OOOoyalaPlayerCurrentItemChangedNotification]) {
        /*
         * The Ooyala player has a bug; sometimes it sends a CurrentItemChangedNotification twice.
         * To workaround this behavior and to avoid creating duplicate views, we check that we've received
         * a "contentTreeReady" notification in order to enter here and create a new view.
         * Doing this we distinguish between fake item changes and actual ones.
         */
        if (showingContent) {
            /*
             * Send start (and stop if needed) if:
             * - If start has not been sent yet
             * or
             * - We've received a contentTreeReady after a start
             */
            if (!self.flags.started || self.contentTreeNotificationReceivedAfterItemChanged) {
                [self fireStop];
                [self fireStart];
                self.contentTreeNotificationReceivedAfterItemChanged = false;
            }
        }
    } else if ([notificationName isEqualToString:OOOoyalaPlayerTimeChangedNotification]) {
        
    } else if ([notificationName isEqualToString:OOOoyalaPlayerPlayStartedNotification]) {
        //Removed due to Ooyala liyng
        /*if (showingContent) {
            [self fireJoin];
        }*/
    } else if ([notificationName isEqualToString:OOOoyalaPlayerStateChangedNotification]) {
        if (showingContent) {
            switch (player.state) {
                case OOOoyalaPlayerStateLoading:
                    if (!self.flags.paused && !self.justSeeked) {
                        //[self fireBufferBegin];
                    }
                    self.justSeeked = false;
                    break;
                case OOOoyalaPlayerStatePaused:
                    [self firePause];
                    break;
                case OOOoyalaPlayerStatePlaying:
                    // If start or join are not sent, send them
                    if (!self.flags.started) {
                        [self fireStart];
                        self.contentTreeNotificationReceivedAfterItemChanged = false;   
                    }
                    
                    if (!self.flags.joined) {
                        [self fireJoin];
                    }
                    
                    //[self fireBufferEnd];
                    [self fireResume];
                    break;
                default:
                    break;
            }
        }
    } else if ([notificationName isEqualToString:OOOoyalaPlayerPlayCompletedNotification]){
        if (showingContent) {
            [self fireStop];
        }
    } else if ([notificationName isEqualToString:OOOoyalaPlayerSeekStartedNotification]){
        if (showingContent) {
            [self fireSeekBegin];
        }
    } else if ([notificationName isEqualToString:OOOoyalaPlayerSeekCompletedNotification]){
        if (showingContent) {
            [self fireSeekEnd];
            self.justSeeked = true;
        }
    } else if ([notificationName isEqualToString:OOOoyalaPlayerErrorNotification]) {
        OOOoyalaError * error = player.error;
        if (error == nil) {
            [self fireFatalErrorWithMessage:@"Unknown error" code:@"9000" andMetadata:nil];
        } else {
            [self fireFatalErrorWithMessage:error.description code:[NSString stringWithFormat:@"%ld", (long)error.code] andMetadata:nil];
        }
    }
}

#pragma mark Override get methods

- (NSNumber*) getPlayhead{
    @try {
        OOOoyalaPlayer * player = self.player;
        if (!player.isShowingAd) {
            double playhead = player.playheadTime;
            if (playhead > 0) {
                self.lastPlayhead = @(playhead);
            }
        }
    } @catch (NSException *exception) {
        [YBLog logException:exception];
    } @finally {
        return self.lastPlayhead;
    }
}

- (NSValue*) getIsLive{
    OOVideo * currentItem = self.player.currentItem;
    if (currentItem != nil) {
        return currentItem.live? @YES : @NO;
    } else {
        return [super getIsLive];
    }
}

- (NSString *)getTitle {
    OOVideo * currentItem = self.player.currentItem;
    if (currentItem != nil) {
        return currentItem.title;
    } else {
        return [super getTitle];
    }
}

- (NSNumber *)getBitrate {
    NSNumber * bitrate = [super getBitrate];
    
    OOVideo * currentItem = self.player.currentItem;
    if (currentItem != nil) {
        OOStream * stream = [OOStream bestStreamFromArray:[currentItem streams]];
        if (stream != nil) {
            long br = stream.combinedBitrate;
            if (br > 0) {
                bitrate = @(br);
            }
        }
    }
    
    return bitrate;
}

- (NSNumber *)getDuration {
    return @(self.player.duration);
}

- (NSString *)getResource {
    
    NSString * resource = [super getResource];
    OOVideo * currentItem = self.player.currentItem;
    if (currentItem != nil) {
        OOStream * stream = [OOStream bestStreamFromArray:[currentItem streams]];
        if (stream != nil) {
            NSURL * url = stream.decodedURL;
            if (url != nil ) {
                resource = url.absoluteString;
            }
        }
    }
    
    return resource;
}

- (NSString *)getPlayerVersion {
    return [Utils getPlayerNameForAds:false];
}

- (NSString *)getPlayerName {
    return [Utils getPlayerNameWithOSForAds:false];
}

- (NSString *)getVersion {
    return [Utils getPlayerNameWithOSAndVersionForAds:false];
}

@end
