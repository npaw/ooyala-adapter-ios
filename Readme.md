# YouboraOoyalaAdapter

A framework that will collect several video events from the Ooyala and send it to the back end

# Installation

#### CocoaPods

Insert into your Podfile

add this sources to your Podfile

```bash
source 'https://bitbucket.org/npaw/ios-sdk-podspecs.git'
source 'https://github.com/ooyala/ios-sdk-podspecs.git'
source 'https://github.com/CocoaPods/Specs.git'
```

```bash
pod 'YouboraOoyalaAdapter'
```

and then run

```bash
pod install
```

#### Manually

## How to use

## Start plugin and options

#### Swift

```swift

//Import
import YouboraLib

...

//Config Options and init plugin (do it just once for each play session)

var options: YBOptions {
        let options = YBOptions()
        options.contentResource = "http://example.com"
        options.accountCode = "accountCode"
        options.adResource = "http://example.com"
        options.contentIsLive = NSNumber(value: false)
        return options;
    }
    
lazy var plugin = YBPlugin(options: self.options)
```

#### Obj-C

```objectivec

//Import
#import <YouboraLib/YouboraLib.h>

...

// Declare the properties
@property YBPlugin *plugin;

...

//Config Options and init plugin (do it just once for each play session)

YBOptions *options = [YBOptions new];
options.offline = false;
options.contentResource = resource.resourceLink;
options.accountCode = @"powerdev";
options.adResource = self.ad?.adLink;
options.contentIsLive = [[NSNumber alloc] initWithBool: resource.isLive];
        
self.plugin = [[YBPlugin alloc] initWithOptions:self.options];
```

For more information about the options you can check [here](http://developer.nicepeopleatwork.com/apidocs/ios6/Classes/YBOptions.html)


### YBOoyalaAdapter & YBOoyalaAdsAdapter

#### Swift

```swift
import YouboraOoyalaAdapter

...

//Once you have your player and plugin initialized you can set the adapter
self.plugin.fireInit()
self.plugin.adapter = YBOoyalaAdapterSwiftTransformer.transform(from: YBOoyalaAdapter(player: player))

...

//If you want to setup the ads adapter
self.plugin.adsAdapter = YBOoyalaAdapterSwiftTransformer.transform(from: YBOoyalaAdsAdapter(player: player))

...

// When the view gonna be destroyed you can force stop and clean the adapters in order to make sure you avoid retain cycles  
self.plugin.fireStop()
self.plugin.removeAdapter()
self.plugin.removeAdsAdapter()
```

#### Obj-C

```objectivec
#import <YouboraOoyalaAdapter/YouboraOoyalaAdapter.h>

...

//Once you have your player and plugin initialized you can set the adapter
[self.plugin fireInit];
[self.plugin setAdapter: [[YBOoyalaAdapter alloc] initWithPlayer:player]];

...

//If you want to setup the ads adapter
[self.plugin setAdsAdapter:[[YBOoyalaAdsAdapter alloc] initWithPlayer:player]];

...

// When the view gonna be destroyed you can force stop and clean the adapters in order to make sure you avoid retain cycles  
[self.plugin fireStop];
[self.plugin removeAdapter];
[self.plugin removeAdsAdapter];
```

## Run samples project

###### Via cocoapods

Navigate to the root folder and then execute: 

```bash
pod update
```

1. Now you have to go to your **target > General > Frameworks, Libraries and Embedded Content** and change the frameworks that you are using in cocoapods from **Embed & Sign** to **Do Not Embed** case you still have problems with duplication symbols just remove the frameworks from **Frameworks, Libraries and Embedded Content**
2. Since Ooyala only supports cocoapods for iOS, tvOS won't work with cocoapods 


###### Via carthage (Default)

Navigate to the root folder and then execute: 
```bash
carthage update 
```

---
**NOTES**

Case you have problems with Swift headers not found, when you're trying to install via **carthage** please do the follow instructions: 

 1. Remove Carthage folder from the project
 2. Execute the follow command ```rm -rf ~/Library/Caches/org.carthage.CarthageKit```

---