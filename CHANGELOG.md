## [6.0.6] - 2020-03-09
### Changed
- Move the version to a static var in order to don't override the app version
## [6.0.5] - 2020-02-26
### Changed
- Change the way like adapter gets the version
### Changed
- Update project to support OoyalaSDK latest version
## [6.0.4] - 2020-02-17
### Added
- Cocoapods support for iOS
### Changed
- Update project to support OoyalaSDK latest version
## [6.0.3] - 2019-10-15
### Fixed
- Thread mix up on fireStop method from wrapper

## [6.0.2] - 2018-10-11
### Added
- Added tvOS suport

## [6.0.1] - 2018-04-04
### Added
- Swift wrapper

## [6.0.0] - 2018-02-16
### Release
- Released version
