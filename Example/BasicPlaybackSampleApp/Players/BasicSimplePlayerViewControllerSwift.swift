//
//  BasicSimplePlayerViewControllerSwift.swift
//  BasicPlaybackSampleApp
//
//  Created by Enrique Alfonso Burillo on 04/04/2018.
//  Copyright © 2018 Ooyala, Inc. All rights reserved.
//

import UIKit
import YouboraOoyalaAdapter
import YouboraConfigUtils

class BasicSimplePlayerViewControllerSwift: SampleAppPlayerViewController {
    fileprivate var ooyalaPlayerViewController: OOOoyalaPlayerViewController?
    fileprivate var embedCode: String?
    fileprivate var nib: String?
    fileprivate var pcode: String?
    fileprivate var playerDomain: String?
    
    fileprivate var options: YBOptions {
        let options = YouboraConfigManager.getOptions()
        return options
    }
    
    fileprivate lazy var plugin = YBPlugin(options: self.options)
    
    override init(playerSelectionOption: PlayerSelectionOption!){
        super.init(playerSelectionOption: playerSelectionOption)
        self.nib = "PlayerSimple";
        self.pcode = "R2d3I6s06RyB712DN0_2GsQS-R-Y";
        self.playerDomain = "http://www.ooyala.com";
        
        if(self.playerSelectionOption != nil){
            self.embedCode = self.playerSelectionOption.embedCode;
            self.title = self.playerSelectionOption.title;
        }
        
        if(self.playerSelectionOption.pcode != nil){
            self.pcode = self.playerSelectionOption.pcode
        }
    }
    
    override func loadView() {
        super.loadView()
        Bundle.main.loadNibNamed(self.nib!, owner: self, options: nil)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let player = OOOoyalaPlayer.init(pcode: self.pcode!, domain: OOPlayerDomain.init(string: self.playerDomain!))
        self.ooyalaPlayerViewController = OOOoyalaPlayerViewController.init(player: player)
        
        NotificationCenter.default.addObserver(self, selector: #selector(notificationHandler(notification:)), name: nil, object: self.ooyalaPlayerViewController?.player)
        
        addChild(self.ooyalaPlayerViewController!)
        self.playerView.addSubview((self.ooyalaPlayerViewController?.view)!)
        self.ooyalaPlayerViewController!.view.frame = self.playerView.bounds
        
        self.ooyalaPlayerViewController?.player.setEmbedCode(self.embedCode!)
        
        YBLog.setDebugLevel(YBLogLevel.verbose)

        self.plugin.fireInit()
        
        self.plugin.adapter = YBOoyalaAdapterSwiftTransformer.transform(from: YBOoyalaAdapter(player: player))

        self.ooyalaPlayerViewController?.player.play()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        if(self.plugin.adapter?.player != nil && !(self.plugin.adapter?.player?.isShowingAd())!){
            if(self.plugin.adapter != nil){
                self.plugin.adapter?.fireStop()
            }else{
                self.plugin.fireStop()
            }
        }
    }
    
    override func didMove(toParent parent: UIViewController?) {
        if parent != nil {
           self.plugin.fireStop()
           self.plugin.removeAdapter()
           self.plugin.removeAdsAdapter()
        }
    }
    
    @objc func notificationHandler(notification: Notification){
        if(notification.name == NSNotification.Name.OOOoyalaPlayerTimeChanged){
            return;
        }
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        
    }
    
    override init(nibName nibNameOrNil: String?, bundle nibBundleOrNil: Bundle?) {
        super.init(nibName: nibNameOrNil, bundle: nibBundleOrNil)
    }
}
