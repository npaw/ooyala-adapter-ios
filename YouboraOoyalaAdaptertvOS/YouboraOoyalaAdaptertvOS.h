//
//  YouboraOoyalaAdaptertvOS.h
//  YouboraOoyalaAdaptertvOS
//
//  Created by Enrique Alfonso Burillo on 27/09/2018.
//  Copyright © 2018 NPAW. All rights reserved.
//

#import <UIKit/UIKit.h>

//! Project version number for YouboraOoyalaAdaptertvOS.
FOUNDATION_EXPORT double YouboraOoyalaAdaptertvOSVersionNumber;

//! Project version string for YouboraOoyalaAdaptertvOS.
FOUNDATION_EXPORT const unsigned char YouboraOoyalaAdaptertvOSVersionString[];

// In this header, you should import all the public headers of your framework using statements like #import <YouboraOoyalaAdaptertvOS/PublicHeader.h>

